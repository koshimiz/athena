/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONREADOUTGEOMETRYR4_STRIPLAYER_ICC
#define MUONREADOUTGEOMETRYR4_STRIPLAYER_ICC

namespace MuonGMR4{
    inline const Amg::Transform3D& StripLayer::toOrigin() const { return m_transform; }
    inline const StripDesign& StripLayer::design() const { return *m_design;}
    inline const IdentifierHash StripLayer::hash() const { return m_hash;}

    inline Amg::Vector3D StripLayer::localStripPos(unsigned int stripNum) const {
      Amg::Vector3D stripPos{Amg::Vector3D::Zero()};
      std::optional<Amg::Vector2D> planePos = m_design->center(stripNum);
      if (!planePos) {          
        return stripPos;
      }
      stripPos.block<2,1>(0,0) = std::move(*planePos);
      return stripPos;
    }
    inline Amg::Vector3D StripLayer::stripPosition(unsigned int stripNum) const {
        return toOrigin() * localStripPos(stripNum);
    }
}
#endif